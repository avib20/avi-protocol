# Building the SNE Protocol Wireshark plugin from source
This file describes how to build the SNE Protocol for Wireshark from source.

## Automatically
On UNIX based systems (like macOS and Linux), you can run `build.py` to
automatically build and install the plugin, or you can skip to the next
section to do it manually.

1. First you need to already have Wireshark installed on your system. If you
   don't, then follow the instructions from step 2 of the README for either
   [Linux](README.md#Linux) or [macOS](README.md#macOS).

2. Open a terminal in `wireshark-plugin` and run `./build.py`.

3. Launch Wireshark. If everything is done correctly, you should be able to see
   the sne plugin when you go to Help → About Wireshark → Plugins.

## Manually
1. First you need to already have Wireshark installed on your system. If you
   don't, then follow the instructions from step 2 of the README for either
   [Linux](README.md#Linux), [macOS](README.md#macOS) or
   [Windows](README.md#Windows).

2. On Windows follow these steps up until 2.2.9 from the [Wireshark wiki](https://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html)
   to be able to build the plugin. On Linux or macOS everything should be either
   preinstalled, or easy to get via your package manager/homebrew.

3. On Linux/macOS you can run [setup-env.sh](setup-env.sh) and skip to step 8,
   this will setup the WireShark git repository in a new folder `Wireshark`.
   If you want you can also follow the next steps to do everything manually. On
   Windows it is required to do everything manually.

4. Clone the git repository of the Wireshark source code using
   `git clone https://code.wireshark.org/review/wireshark`. This will create a
   directory called `wireshark`.

5. In the `wireshark` directory execute `git checkout wireshark-x.y.z` where
   x.y.z matches your version of Wireshark.

   _Note:_ If you don't know your version of Wireshark you can run
   `wireshark --version`.

6. Copy the `sne` folder to `wireshark/plugins/epan/`

7. Windows only: Add `plugins/epan/sne` to the `PLUGIN_SRC_DIRS` in
   `CMakeLists.txt` located in the root of the git repository. This should be
   located around line 1400, but this can differ from versions.

8. Linux/macOS: Create a directory called `build` at the parent directory of the git
   repository and run the following command:
   `cmake -G Ninja ../wireshark -DCUSTOM_PLUGIN_SRC_DIR="plugins/epan/sne" && ninja`

   Windows: Run steps 2.2.11 up until 2.2.13 (except step 4 and 5 of 2.2.13)
   from the [Wireshark wiki](https://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html#ChSetupPrepareCommandCom)

9. Linux/macOS: Locate the plugin in the `build` directory at
   `run/plugins/x.y/epan/sne.so` where x.y matches your Wireshark version x.y.z.

   Windows: Locate the plugin at `C:\Development\wsbuild64\run\RelWithDebInfo\plugins\x.y\epan\sne.dll`
   where x.y matches your Wireshark version x.y.z.

10. Linux: Put the plugin in the folder
    `~/.local/lib/wireshark/plugins/x.y/epan/`, where x.y matches your Wireshark
    version x.y.z. If this directory doesn't exist yet you have to create it
    manually.

    macOS: Put the plugin in the folder
    `%APPDIR%/Contents/PlugIns/wireshark/x.y/epan/`, where x.y matches your
    Wireshark version x.y.z. If this directory doesn't exist yet you have to
    create it manually.

    Windows: Put the plugin in the folder
    `%APPDATA%\Wireshark\plugins\3.2\epan\`, where x.y matches your
    Wireshark version x.y.z. If this directory doesn't exist yet you have to
    create it manually.

11. Launch Wireshark. If everything is done correctly, you should be able to see
    the sne plugin when you go to Help → About Wireshark → Plugins.
