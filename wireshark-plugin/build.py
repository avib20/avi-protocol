#!/usr/bin/env python3
import os
import subprocess
import shutil

def get_wireshark_version():
    proc = subprocess.run(["wireshark", "--version"], stdout=subprocess.PIPE, check=True)
    stdout = proc.stdout.decode("utf-8")
    return int(stdout[10]), int(stdout[12])

def setup_env():
    subprocess.run(["setup-env.sh"])

def build(dir="build", src="../wireshark"):
    if not os.path.isdir(dir):
        mkdir(dir)

    subprocess.run(["cmake", "-G", "Ninja", "../wireshark", "-DCUSTOM_PLUGIN_SRC_DIR=\"plugins/epan/sne\""], check=True, cwd=dir)
    subprocess.run(["ninja", check=True, cwd=dir])

def cp_plugin(dir="build", version=(2, 6))
    x, y = version
    if sys.platform == "darwin":
        path = "{}/Contents/PlugIns/wireshark/{}.{}/epan".format(os.getenv('APPDIR'), x, y)
    else:
        path = "{}/.local/lib/wireshark/plugins/{}.{}/epan".format(os.getenv('HOME'), x, y)
    src = "{}/run/plugins/{}.{}/epan/sne.so".format(dir, x, y)
    filename = "{}/sne.so".format(path)
    os.makedirs(path, exist_ok=True)
    shutil.copyfile(src, filename)

if __name__ == "__main__":
    setup_env()
    build()
    version = get_wireshark_version()
    cp_plugin(version=version)
