# SNE Protocol Wireshark plugin
This plugin provides support for the SNE protocol in Wireshark.
You can either download the virtual machine and run Wireshark with the plugin
preinstalled from there. Or you can install the plugin yourself by following
the instructions of [Installing](#Installing).

## Virtual Machine

1. Install [VirtualBox](https://www.virtualbox.org/), in Ubuntu/Debian this
   can be done with the command `sudo apt update && sudo apt install virtualbox`.

2. Go to the [Virtual Machines](https://gitlab.com/avib20/avi-protocol/-/wikis/Virtual-Machine)
   page on the wiki and download `sne.ova`.

3. Open VirtualBox and click on File → Import Appliance and import `sne.ova`.

4. Start the VM and login with password `sne`.

5. Double-click the capture file on the desktop to start Wireshark and analyze
   the capture file.

## Installing

This section describes how to acquire a prebuilt version of the plugin. Follow
the instruction for either [Linux](#Linux), [macOS](#macOS) or
[Windows](#Windows).

_Note:_ There are only prebuilt versions for the amd64 architecture, for other
computer architectures you will have to build from source.

_Note:_ If you want to build the plugin from source, download the git repository
and see [INSTALL.md](INSTALL.md).

### Linux
_Note:_ The prebuilt plugin has only been tested on Ubuntu 18.04, if you run
into a problem with a different distribution, you can try to build it manually
with the instructions in [INSTALL.md](INSTALL.md).
1. If you already have Wireshark installed go to step 3, otherwise go to step 2.

2. First you need to install Wireshark. On Ubuntu 18.04 up until at least Ubuntu 20.04
   you can run `sudo apt update && sudo apt install wireshark` and go to step 4.
   If you use another distribution, then follow the instructions for your
   distribution at the [Wireshark wiki](https://www.wireshark.org/docs/wsug_html_chunked/ChBuildInstallUnixInstallBins.html).
   Make sure you either install version 2.6.x, 3.0.x or 3.2.x, as these are the only
   versions that we provide prebuilt versions of the plugin for. You can now go
   to step 4.

   _Note:_ If it is not possible to acquire this version in your distribution
   you can follow the steps from the [Wireshark wiki](https://www.wireshark.org/docs/wsug_html_chunked/ChBuildInstallUnixBuild.html)
   to install an older version.

3. _If you use Ubuntu 18.04, 18.10, 19.04, 19.10 or
   20.04, you can go to the next step._

   Run `wireshark --version` and make sure that you got either version 2.6.x,
   3.0.x or 3.2.x. If this is not the case either update your version of
   Wireshark to one of these versions, or uninstall Wireshark and go to step 2.

4. Download and run the [python install script](https://gitlab.com/avib20/avi-protocol/blob/master/wireshark-plugin/install-plugin.py)
   and go to step 7. Or install the plugin manually by following the next steps.

5. _Optional_: Download the plugin corresponding to your version of Wireshark
   from the Releases page on [GitLab](https://gitlab.com/avib20/avi-protocol/-/releases).

6. _Optional_: Put the downloaded plugin in the folder
   `~/.local/lib/wireshark/plugins/x.y/epan/`, where x.y matches your Wireshark
   version x.y.z. If this directory doesn't exist yet you have to create it
   manually.

7. Launch Wireshark. If everything is done correctly, you should be able to see
   the sne plugin when you go to Help → About Wireshark → Plugins.

### macOS
_Note:_ The prebuilt plugin has not been tested on macOS, if you run into a
problem you can try to build it from source with the instructions in
[INSTALL.md](INSTALL.md).
1. If you already have Wireshark installed go to step 3, otherwise go to step 2.

2. First you need to install Wireshark. Get the macOS installer from the
   [Wireshark download](https://www.wireshark.org/download.html) page and follow
   the instructions for macOS at the [Wireshark wiki](https://www.wireshark.org/docs/wsug_html_chunked/ChBuildInstallOSXInstall.html).
   Make sure you install version 3.2.x, as this is the only version
   that we provide a prebuilt version of the plugin for. You can now go to step
   4.

3. Run `wireshark --version` and make sure that you got version 3.2.x. If this
   is not the case either update your version of Wireshark to one this
   version, or uninstall Wireshark and go to step 2.

4. Download and run the [python install script](https://gitlab.com/avib20/avi-protocol/blob/master/wireshark-plugin/install-plugin.py)
   and go to step 7. Or install the plugin manually by following the next steps.

5. _Optional_: Download the plugin corresponding to your version of Wireshark
   from the Releases page on [GitLab](https://gitlab.com/avib20/avi-protocol/-/releases).

6. _Optional_: Put the downloaded plugin in the folder
   `%APPDIR%/Contents/PlugIns/wireshark/3.2/epan/`. If this directory doesn't
   exist yet you have to create it manually.

7. Launch Wireshark. If everything is done correctly, you should be able to see
   the sne plugin when you go to Help → About Wireshark → Plugins.

### Windows
1. If you already have Wireshark installed go to step 3, otherwise go to step 2.

2. First you need to install Wireshark. Follow the instructions for windows at
   the [Wireshark wiki](https://www.wireshark.org/docs/wsug_html_chunked/ChBuildInstallWinInstall.html).
   Make sure you install version 3.2.x, as this is the only version that we
   provide a prebuilt version of the plugin for. You can now go to step 4.

3. Check that your version of Wireshark is 3.2.x. If this is not the case either
   update your version of Wireshark to this version, or uninstall Wireshark and
   go to step 2.

4. Download and run the [python install script](https://gitlab.com/avib20/avi-protocol/raw/master/wireshark-plugin/install-plugin.py?inline=false)
   and go to step 7. Or install the plugin manually by following the next steps.

5. _Optional_: Download the plugin corresponding to your version of Wireshark
   from the Releases page on [GitLab](https://gitlab.com/avib20/avi-protocol/-/releases).

6. _Optional_: Put the downloaded plugin in the folder
   `%APPDATA%\Wireshark\plugins\3.2\epan\`. If this directory doesn't exist yet
   you have to create it manually.

7. Launch Wireshark. If everything is done correctly, you should be able to see
   the sne plugin when you go to Help → About Wireshark → Plugins.
