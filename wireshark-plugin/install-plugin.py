#!/usr/bin/env python3
import os
import sys
import subprocess
import urllib.request
import shutil

def _get_wireshark_exe_nt():
    exe = "C:\\Program Files\\Wireshark\\wireshark.exe"
    while not os.path.exists(exe):
        exe = input("The Wireshark executable could not be found at {}. "
                    "Type location of Wireshark executable: ".format(exe))
    return exe

def get_wireshark_version():
    if os.name == "nt":
        com = _get_wireshark_exe_nt()
    else:
        com = "wireshark"

    proc = subprocess.run([com, "--version"], stdout=subprocess.PIPE, check=True)
    stdout = proc.stdout.decode("utf-8")
    return int(stdout[10]), int(stdout[12])

def download_plugin(link, version):
    x, y = version

    if os.name == "nt":
        path = "{}\\Wireshark\\plugins\\{}.{}\\epan".format(os.getenv('APPDATA'), x, y)
        filename = "{}\\sne.dll".format(path)
    else:
        if sys.platform == "darwin":
            path = "{}/Contents/PlugIns/wireshark/{}.{}/epan".format(os.getenv('APPDIR'), x, y)
            filename = "{}/sne.so".format(path)
        else:
            path = "{}/.local/lib/wireshark/plugins/{}.{}/epan".format(os.getenv('HOME'), x, y)
            filename = "{}/sne.so".format(path)

    os.makedirs(path, exist_ok=True)

    with urllib.request.urlopen(link) as response, open(filename, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)

if __name__ == "__main__":
    base_url = "https://drive.google.com/uc?export=download&id={}"

    if os.name == "nt":
        loc = {(3, 2): base_url.format("11gKclFDYopF5n9Gm4ZJLgwfIk_vNoRNE")}
    elif os.name == "posix":
        loc = {
            (2, 6): base_url.format("1PXykWnReETWiLqw0N1W6dOPEbiXlmykJ"),
            (3, 0): base_url.format("15Pfa4RtGP9TW3uFbWhkwM5qWqXvEcyRF"),
            (3, 2): base_url.format("146rGPuTqWsDxIp1c4jJrJcRd0A8mdeMF")
        }
    else:
        exit("Unknown OS")

    version = get_wireshark_version()

    if version not in loc:
        exit("Unsupported wireshark version")

    download_plugin(loc[version], version)
