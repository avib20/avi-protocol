#!/bin/sh
git clone https://code.wireshark.org/review/wireshark
(cd wireshark; git checkout wireshark-$(wireshark --version | sed -n '1p' | grep -Po 'Wireshark \K[^ ]+'))
cp -r sne wireshark/plugins/epan
echo "Copied sne to wireshark/plugins/epan"
