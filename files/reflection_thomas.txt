Title: Reflection Thomas
Category: 6
Protocol: custom

Over the last few years, usage of the systems and networks has been changing
dramatically. Usage of the Internet, the biggest network we know, has been
rising since its inception, as many more people and devices are connected to
this network. The internet not only provides a source of information or
entertainment, but also allows for more connected world in
terms of science. We can now run tasks on computers hosted elsewhere that are
too complex or difficult for personal computers. These computer clusters
are designed to be used by multiple people, and have to be powerful enough to
run difficult tasks.
Besides the growth of clustered systems, we also see a rise in embedded
systems, where computers control much of everyday tasks in a variety of ways.

In this light, I believe that the research that the Systems and Networking Lab
is very important for developing new solutions in this changing landscape. SNE
has a broad focus. It covers networks, embedded systems, parallel computing,
cloud services, cluster computers, and data flows. All these subjects are very
important in the digitized and connected world we have today.
