Title: Scientific context for SNE
Category: 1
Protocol: custom

The Systems and Networking (SNE) lab has a varied location on the
'map of computer science'. Of course it contains the section 'Networking',
but it doesn't stop there. SNE is also engaged in research in the
field of 'Hacking', 'Big Data' and 'The internet of things'.
Nowadays all these parts are covered in abundant literature.

For example, there is a great deal of emphasis on the
security of the four layers of the network,
by developing new protocols or strengthening existing ones.
In particular, new techniques, such as those described in
Web 3.0 and Web 4.0, are often still open to exploits.
With the research done in the SNE,
the research group contributes to securing and improving the network.

link to the map of computer science:
https://go.aws/36R8jFD