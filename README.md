# SNE Protocol
We created a new protocol for an assignment of Academische Vaardigheden.
For a more detailed information see the
[wiki](https://gitlab.com/avib20/avi-protocol/-/wikis/).

## Installation
For information on how to install the plugin, see the
[Plugin README](wireshark-plugin/README.md).

A pre-generated capture-file can be found at the
[wiki](https://gitlab.com/avib20/avi-protocol/-/wikis/Capture-file).


## License
This is free and unencumbered software released into the public domain, for more
information see [LICENSE](LICENSE).
