###############################################################################
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <https://unlicense.org>
###############################################################################
# Simple HTTP response generator
from http import HTTPStatus
from email.utils import formatdate
import sys

VERSION = "HTTP/1.1"
SERVER = "Python {version} (custom)".format(version=sys.version).replace('\n',
                                                                         ' ')


def generate_header(code: HTTPStatus, type: str, length: int) -> str:
    """Generates a HTTP response header

    :param code: Status code of the HTTP response
    :param type: Content-Type of the body
    :param length: Length of the body
    :return: HTTP response header
    """
    return "{version} {code.value} {code.phrase}\r\n" \
           "Date: {date}\r\n" \
           "Server: {server}\r\n" \
           "Content-Type: {type}\r\n" \
           "Connection: {connection}\r\n" \
           "Content-Length: {length}".format(
        version=VERSION, code=code,
        date=formatdate(usegmt=True), server=SERVER,
        type=type, connection="close", length=length
    )


def generate_http(body: str, type: str = "text/plain; charset=UTF-8",
                  status: HTTPStatus = HTTPStatus.OK) -> bytes:
    """Generates a HTTP response

    :param body: The body of the HTTP response
    :param type: Content-Type of the HTTP response, text/plain by default
    :param status: Status code of the HTTP response
    :return: HTTP response
    """
    body = body.replace('\n', '\r\n')
    return "{header}\r\n\r\n{body}".format(
        header=generate_header(status, type,
                               len(body.encode("utf-8"))),
        body=body).encode("utf-8")
