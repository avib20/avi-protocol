#!/usr/bin/env python3
###############################################################################
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <https://unlicense.org>
###############################################################################
# This file generates the packets to be captured by WireShark. To get the help
# file near the top of the capture list you must follow the following steps:
# 1. Start this script
# 2. Prepare a terminal with the command `nc localhost 8080`
# 3. Start capturing WireShark
# 4. Start the command in the terminal
import socket
import warnings
from typing import List, Optional, Union
from random import choice, uniform
from time import sleep
from generate_http import generate_http
from generate_custom import generate_sne, generate_sne_bin, Categories, Types

PORT = 3056  # Port used to broadcast UDP datagrams


class Data:
    """Container for messages to be sent"""

    def __init__(self, title: str, category: Categories, protocol: str,
                 type: Types = Types.TEXT,
                 paragraphs: Optional[Union[List[str], str]] = None,
                 other_data: Optional[bytes] = None):
        """Initializes a Data object

        :param title: Title of the message
        :param category: Category of the text as defined in Categories
        :param protocol: Protocol to use, supported: 'custom', 'http'
        :param type: Type of data, as defined in Types
        :param paragraphs: Optional, if protocol is 'custom', this needs to be
        a list of paragraphs to be sent. If the protocol is 'http', this must
        be a str
        :param other_data: Optional, can be used for binary data instead of
        paragraphs. Protocol must be 'custom'
        """
        self.title = title
        self.category = category
        self.protocol = protocol
        self.paragraphs = paragraphs
        self.other_data = other_data
        self.type = type

    def _generate_binary_message(self) -> bytes:
        return generate_sne_bin(self.other_data, self.title, self.category,
                                self.type)

    def generate_messages(self) -> List[bytes]:
        """Generates a list of messages from the paragraphs

        :return: List of messages
        """
        messages = []

        if self.protocol == "custom":
            if self.type != Types.TEXT:
                messages.append(self._generate_binary_message())
            else:
                for i, paragraph in enumerate(self.paragraphs):
                    messages.append(
                        generate_sne(paragraph, self.title, self.category,
                                     i, len(self.paragraphs) - 1))
        else:
            messages.append(generate_http(self.paragraphs))

        return messages

    @classmethod
    def read_binary(cls, filename: str, title: str, category: Categories,
                    type: Types) -> 'Data':
        """Generates a Data object from a binary file

        :param filename: Name of the file
        :param title: Title of the Data object
        :param category: Category of the Data object
        :param type: Type of the data object
        :return: Data object
        """
        with open(filename, "rb") as image:
            f = image.read()

        return cls(title, category, 'custom', type, other_data=f)

    @classmethod
    def read_file(cls, filename: str) -> 'Data':
        """Generates a Data object from a file

        The file must follow the structure of `../files/TEMPLATE.txt`

        :param filename: Name of the file
        :return: Data object
        """
        title = None
        category = None
        protocol = None
        with open(filename) as f:
            for line in f:
                if line == '\n':  # Header of file has ended
                    break

                # Split line in key/value
                line = line.split(':')

                if line[0].strip().lower() == "title":
                    title = line[1].strip()
                elif line[0].strip().lower() == "category":
                    category = Categories(int(line[1].strip()))
                elif line[0].strip().lower() == "protocol":
                    protocol = line[1].strip()

            # Get rest of file
            text = f.read()

            # Check if all parameters are found
            if title is None:
                warnings.warn("Title was not found in {}. "
                              "Defaulting to empty title".format(filename),
                              RuntimeWarning)
                title = ""
            if category is None:
                warnings.warn("Category was not found in {}. "
                              "Defaulting to '{}'".format(filename,
                                                          Categories(0).name),
                              RuntimeWarning)
                category = Categories(0)
            if protocol is None:
                warnings.warn("Title was not found in {}. "
                              "Defaulting to 'custom'".format(filename),
                              RuntimeWarning)
                protocol = "custom"

            # split paragraphs in sequences if protocol is 'custom'
            if protocol == "custom":
                paragraphs = text.split('\n\n')
            else:
                paragraphs = text
            return cls(title, category, protocol, paragraphs=paragraphs)


def generate_random_queue(messages: List[List[bytes]]) -> List[bytes]:
    """Puts messages in random order, but keeps sequences in order

    Warning: removes items from messages

    :param messages: List of sequences
    :return: List of messages in random order
    """
    queue = []
    items = len(messages)

    while items > 0:
        sequences = choice(messages)

        if len(sequences) < 1:
            messages.remove(sequences)
            items -= 1
            continue

        queue.append(sequences.pop(0))

    return queue


def send_tcp(data: bytes) -> None:
    """Sends a message via tcp to `127.0.0.1` on port 8080

    There is no client written to receive the data. To let the python script
    continue you can run `nc localhost 8080`

    :param data: Message to be sent
    """
    ip = '127.0.0.1'
    port = 8080

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    s.bind((ip, port))
    s.listen(1)

    conn, addr = s.accept()
    conn.send(data)
    conn.close()


def send_udp(socket: socket.socket, data: bytes) -> None:
    """Sends a broadcast message via udp on port PORT

    :param socket: Socket used to send the message
    :param data: Message to be sent
    """
    socket.sendto(data, ('<broadcast>', PORT))


def send_random(socket: socket.socket, queue: List[bytes], start: float = 0.0,
                stop: float = 2.0) -> None:
    """Sends messages in queue with a delay between start and stop

    :param socket: Socket used to send the message
    :param queue: The queue containing the messages
    :param start: Minimum delay between messages, 0.0 by default
    :param stop: Maximum delay between messages, 2.0 by default
    """
    for message in queue:
        sleep(uniform(start, stop))
        send_udp(socket, message)


if __name__ == '__main__':
    import sys
    from random import seed

    file_dir = "../files/"
    help = "../files/help.txt"

    files = [
        "general_info.txt",
        "research_group_cci.txt",
        "research_group_mns.txt",
        "research_group_pcs.txt",
        "reflection_skip.txt",
        "protocol_design.txt",
        "packet_generator.txt",
        "wireshark_plugin.txt",
        "future.txt",
        "reflection_herman.txt",
        "reflection_jurre.txt",
        "social_context.txt",
        "scientific_context.txt",
        "pumpkin.txt",
        "weevilscout.txt",
        "publication_1.txt",
        "publications.txt",
        "publication_2.txt",
        "reflection_tristan.txt",
        "reflection_thomas.txt",
        "reflection_dex.txt"
    ]

    image = "../files/ferrari.png"

    messages = []

    # seed random generator
    seed()

    # Generate UDP socket
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM,
                           socket.IPPROTO_UDP)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    server.bind(("", PORT))

    # Send help file first
    data = Data.read_file(help)
    message = data.generate_messages()[0]
    print("Opening TCP connection, execute `nc localhost {}`"
          " in a new terminal to continue...".format(8080), file=sys.stderr)
    send_tcp(message)

    # Read files and collect messages
    for file in files:
        file = file_dir + file

        try:
            data = Data.read_file(file)
        except Exception as e:
            print(e, file=sys.stderr)
            print("Skipping file {}...".format(file), file=sys.stderr)
            continue

        messages.append(data.generate_messages())

    # Add image to messages
    image_data = Data.read_binary(image, "Red Ferrari", Categories.FERRARI,
                            Types.PNG)
    messages.append(image_data.generate_messages())

    # Randomize queue and send with random pauses
    queue = generate_random_queue(messages)
    print("Sending UDP messages...", file=sys.stderr)
    send_random(server, queue)
