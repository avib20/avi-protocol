###############################################################################
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <https://unlicense.org>
###############################################################################
# Generates messages of the custom SNE Protocol
# The format is defined as followed: The first 32 bits are the identifier to
# detect the protocol. The next 4 bits define the category, the 4 bits after
# that define the content type. Next are 8 bits used for the splitting of
# packets. The first 4 bits contain the sequence number, and the last 4 bits
# the total amount of sequences. The next 16 bits define the length of the
# content in bytes. Following are 32 bytes for the title of the message. And
# lastly there are a variable number of bytes for the actual content. The
# maximum length of the content is 65,464 bytes and the content is padded to a
# multiplicative of 32 bits.
from enum import Enum
import struct
import math

IDENTIFIER = [b'\x53', b'\x4e', b'\x45', b'\x70']
FORMAT = "!4cBBH32s{length}s"


class Types(Enum):
    TEXT = 0
    PNG = 1


class Categories(Enum):
    SNE = 0
    CONTEXT = 1
    SOFTWARE = 2
    PUBLICATIONS = 3
    FUTURE = 4
    PROGRAM = 5
    REFLECTION = 6
    FERRARI = 7


def calc_length(string: bytes) -> int:
    """Calculates the length of the content

    :param string: Content of which the length is desired
    :return: The length of the content in bytes padded to a multiplicative of
    32 bits
    """
    return math.ceil(max(len(string), 1) / 4) * 4


def encode(a: int, b: int) -> int:
    """Stores two 4-bit integers in 8 bits

    :param a: first 4-bit integer
    :param b: second 4-bit integer
    :return: 8-bit integer containing a and b
    """
    # make sure that both integers are 4-bit
    a = abs(a) & 15
    b = abs(b) & 15

    return (a << 4) + b


def generate_sne(data: str, title: str, category: Categories,
                 sequence: int = 0, total: int = 0) -> bytes:
    """Generates a bytes object following the format of the SNE protocol

    :param data: Content of the SNE message
    :param title: Title of the SNE message
    :param category: Category of the SNE message
    :param sequence: Sequence number of the SNE message
    :param total: Amount of sequences of the complete message
    :return: bytes object following the format of the SNE protocol
    """
    cat_type = encode(category.value, Types.TEXT.value)
    sequences = encode(sequence, total)
    byte_title = title.encode("utf-8").ljust(32, b"\0")  # pad with zeros
    byte_data = data.encode("utf-8")
    length = calc_length(byte_data)
    byte_data = byte_data.ljust(length, b"\0")  # pad with zeros
    struct_fmt = FORMAT.format(length=length)

    return struct.pack(struct_fmt, *IDENTIFIER, cat_type, sequences, length,
                       byte_title, byte_data)


def generate_sne_bin(data: bytes, title: str, category: Categories,
                     type: Types) -> bytes:
    """Generates a bytes object following the format of the SNE protocol

        :param data: Content of the SNE message
        :param title: Title of the SNE message
        :param category: Category of the SNE message
        :return: bytes object following the format of the SNE protocol
        """
    cat_type = encode(category.value, type.value)
    sequences = encode(0, 0)
    byte_title = title.encode("utf-8").ljust(32, b"\0")  # pad with zeros
    length = calc_length(data)
    padded_data = data.ljust(length, b"\0")  # pad with zeros
    struct_fmt = FORMAT.format(length=length)

    return struct.pack(struct_fmt, *IDENTIFIER, cat_type, sequences, length,
                       byte_title, padded_data)
